# README 

## Text Classification 

This project is the fourth project in my program at OpenClassroom. The aim of this project is to automatically recommend tags for a StackOverflow question. I used classical text classification approach, but also an approach based on the LDA method.